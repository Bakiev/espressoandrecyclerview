### An example project which demonstrates an Espresso 3.4.0-rc01 issue for the latest (1.2.1) RecyclerView library

The issue happens after bumping to the latest AndroidX [Recycler View library 1.2.1](https://developer.android.com/jetpack/androidx/releases/recyclerview#1.2.1)

### How to reproduce

- Run `./gradlew cAT`
- `java.lang.RuntimeException: Found more than one sub-view matching  Position matching` is reported

Looks like the `RecyclerViewActions::itemsMatching` [should be updated](https://github.com/android/android-test/blob/9d25d24ec7abe5021672c0423a992e87b44eb261/espresso/contrib/java/androidx/test/espresso/contrib/RecyclerViewActions.java#L453) since new `Adapter` member `mBindingAdapter` is not cleared during `Adapter::onViewRecycled`

### Notes

- Introduced in RecyclerView 1.2.1, works just fine with RecyclerView 1.1.0
