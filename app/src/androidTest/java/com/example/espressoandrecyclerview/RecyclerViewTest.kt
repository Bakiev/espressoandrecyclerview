package com.example.espressoandrecyclerview

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.activityScenarioRule
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test

class RecyclerViewTest {

    @get:Rule
    internal val rule = activityScenarioRule<MainActivity>()

    @Test
    fun findItem() {
        Espresso
            .onView(ViewMatchers.isAssignableFrom(RecyclerView::class.java))
            .perform(RecyclerViewActions.scrollToHolder(layoutPositionMatcher(0)))
    }

    private fun <VH : RecyclerView.ViewHolder?> layoutPositionMatcher(layoutPosition: Int): Matcher<VH> =
        object : TypeSafeMatcher<VH>() {
            override fun matchesSafely(item: VH): Boolean = if (item != null) {
                item.layoutPosition == layoutPosition
            } else {
                false
            }

            override fun describeTo(description: Description) {
                description.appendText(" Position matching ")
            }
        }
}
